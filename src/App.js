import React from 'react';
import WeatherDay from "./components/WeatherDay";
import SelectedDay from "./components/SelectedDay";
import Search from "./components/Search";
import { Grid, Row } from "react-bootstrap";
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weather:"",
            selectedDay:null,
            validationState:"",
        };
    }

    // Filtering the results object to show only specific selected hour.
    filterBy(list, hour=14) { //If not define specific hour default is 2PM. ES6
        return list.filter(function (value) {
            let time = new Date(value.dt*1000);
            return time.getHours() === hour;
        });
    }

    componentDidMount() {
        //Trying to get the last local storage item
        const lastVisit = localStorage.getItem("lastVisit");

        // Check is there is localStorage object and if it's made less than 30mins ago
        // Date.now return unix time in ms. ms/60000 == min unit
        if (lastVisit && ((Date.now() - JSON.parse(lastVisit).timestamp)/60000 < 30)) {
            this.setState(JSON.parse(lastVisit).state);
            console.log("Using local storage.\nlast update was",((Date.now() - JSON.parse(lastVisit).timestamp)/60000).toFixed(2), "minutes ago");
        }
        // In case there is no local storage available OR local storage object age > 30 mins:
        else {
            localStorage.removeItem("lastVisit");
            console.log("Fetching new weather data");
            this.getWeatherDataFrom();
        }
    }

    getWeatherDataFrom = (city="jerusalem") => {
        // Israel ID: 294640
        // Jerusalem ID: 293198
        this.setState({validationState:""});
        const api_key = "ca4582e8e7f01da0f541e466fb8bfa8a";
        const base_url_by_city = `https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&APPID=${api_key}`;
        fetch(base_url_by_city)
            .then(response => response.json())
            .then(object => this.setState({
                weather: object,
                selectedDay:this.filterBy(object.list, 14)[0],
                validationState:"success"
            }))
            .catch(err => {
                this.setState({validationState:false});
                console.log(err);
            })
            .finally(() => localStorage.setItem("lastVisit", JSON.stringify({
                state: this.state,
                timestamp: Date.now()
            })));
    };

    updateSelectedDay = (data) => {
        this.setState({
            selectedDay:data
        })
    };

    render() {
        const weatherDayList = this.state.weather ? this.filterBy(this.state.weather.list, 14).map(day =>
            <WeatherDay key={day.dt} weatherData={day} userChooseWeatherDay={this.updateSelectedDay}/>) : <WeatherDay/>;
        const placeName = this.state.weather ? {
                city: this.state.weather.city.name,
                origin: this.state.weather.city.country
            } : "Jerusalem";

        return  (
            <Grid>
                <SelectedDay weatherData={this.state.selectedDay} placeName={placeName}>
                    <Search updateLocation={this.getWeatherDataFrom} validationState={this.state.validationState}/>
                    {/*<button onClick={() => localStorage.clear()}>Clear localStorage</button>*/}
                </SelectedDay>
                <Row>
                    {weatherDayList}
                </Row>
            </Grid>
        )
    }
}

export default App;
