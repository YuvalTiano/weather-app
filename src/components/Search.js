import React from 'react';
import PropTypes from "prop-types";
import { FormGroup, FormControl } from 'react-bootstrap';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter:"",
        }
    }

    static propTypes = {
        updateLocation: PropTypes.func,
        validationState: PropTypes.oneOf(),
    };

    handleChange = (event) => {
        this.props.updateLocation(event.target.value)
    };

    render() {
        const validateState = this.props.validationState ? "success" : "error";
            return (
            <FormGroup validationState={validateState}>
                <FormControl onChange={this.handleChange} type="text" placeholder="Find a city..." />
                {this.props.validationState ? <FormControl.Feedback /> : null}
            </FormGroup>
        )
    }
}

export default Search;
