import React from 'react';
import PropTypes from 'prop-types';
import Loader from "../HOC/Loader";
import { Row, Col } from "react-bootstrap";
import "./SelectedDay.css";

class SelectedDay extends React.Component {
    static propTypes = {
        weatherData: PropTypes.object.isRequired,
        placeName:PropTypes.shape({
            city: PropTypes.string,
            origin: PropTypes.string
        }),
    };

    render() {
        const city = this.props.placeName.city.split(" ")[0];
        const origin = this.props.placeName.origin;
        const data = this.props.weatherData;
        const temperature = data.main.temp.toFixed(0);
        const windSpeed = (data.wind.speed * 3.6).toFixed(1);
        const weatherDescription = data.weather[0].description;
        const pressure = data.main.pressure.toFixed(0);
        const humidity = data.main.humidity;

        return (
            <Row className="current-weather">
                <Col xs={12} md={4}>
                    <div className="current-place">{city},<div className="origin">{origin}</div></div>
                    <div className="weather-description">{weatherDescription}</div>
                    <div className="current-temperature">{temperature}&deg;c</div>
                </Col>
                <Col xs={6} md={4}>
                    <div className="info">
                        <div className="parameter">Humidity</div>
                        <div className="value">{humidity}%</div>
                        <div className="parameter">Wind</div>
                        <div className="value">{windSpeed}KM/h</div>
                        <div className="parameter">Pressure</div>
                        <div className="value">{pressure}hPs</div>
                    </div>
                </Col>
                <Col xs={6} md={4}>
                    {this.props.children} {/*Search Component*/}
                </Col>
            </Row>
        )
    }
}

export default Loader(SelectedDay, "weatherData");
