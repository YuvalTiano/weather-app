import React from "react";
import PropTypes from "prop-types";
import Loader from "../HOC/Loader";
import { Col , Image } from "react-bootstrap";
import "./WeatherDay.css";

class WeatherDay extends React.Component {
    static propTypes = {
        weatherData: PropTypes.object.isRequired,
    };

    getFormattedTime(timeStamp) {
        let time = new Date(timeStamp*1000);
        // let hour = time.getHours().toString().length === 1 ? `0${time.getHours()}` : time.getHours();
        // let minute = time.getMinutes().toString().length === 1 ? `0${time.getMinutes()}` : time.getMinutes();
        let day = time.getDate().toString().length === 1 ? `0${time.getDate()}` : time.getDate();
        let month = time.getMonth().toString().length === 1 ? `0${time.getMonth()+1}` : time.getMonth()+1;
        return `${day}/${month}`;
    }

    getFormattedDayAtWeek(timeStamp) {
        let day = new Date(timeStamp*1000);
        switch (day.getDay()) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            default:
                return "------";
        }
    };

    getWeatherImg(code) {
        //Thunderstorm: 2xx
        if (200 <= code && code <= 299) {
            return "thunderstorm.gif";
        }
        //Drizzle: 3xx
        else if (300 <= code && code <= 399) {
            return "thunderstorm.gif";
        }
        // No 4xx group!
        //Rain: 5xx
        else if (500 <= code && code <= 599) {
            return "rain.gif";
        }
        //Snow: 6xx
        else if (600 <= code && code <= 699) {
            return "rain.gif";
        }
        //Atmosphere 7xx
        else if (700 <= code && code <= 799) {
            return "rain.gif";
        }
        //Clear 800
        else if (code === 800) {
            return "sunny.gif";
        }
        //Clouds: 80x
        else if (800 < code && code <= 809) {
            return "clouds.gif";
        }
        //Extreme 90x
        else if (900 <= code && code <= 909) {
            return "thunderstorm.gif";
        }
        //Additional 9xx (over 950)
        else if (950 <= code && code <= 999) {
            return "rain.gif";
        }
        else {
            console.error("No ID FOUND");
        }
    }

    handleMouseHover = () => {
        this.props.userChooseWeatherDay(this.props.weatherData);
    };

    render() {
        const data = this.props.weatherData;
        const imgSrc = `${process.env.PUBLIC_URL}/weather-icons/${this.getWeatherImg(data.weather[0].id)}`;
        const dayAtWeek = this.getFormattedDayAtWeek(data.dt);
        const date = this.getFormattedTime(data.dt);
        const temperature = data.main.temp.toFixed(0);

        return (
            <Col sm={12} md={2}>
                <div className="weather-day" onMouseEnter={this.handleMouseHover}>
                    <Image src={imgSrc} responsive alt="sunny"/>
                    <div>{dayAtWeek}</div>
                    <div className="details">
                        <div className="temperature">{temperature}&deg;c</div>
                        <div className="other-details">{date}</div>
                    </div>
                </div>
            </Col>
        );
    }
}

export default Loader(WeatherDay, "weatherData");
