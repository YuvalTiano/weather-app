import React from 'react';
import "./Border.css";

const Border = (ReactComponent) => {
    return class Border extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                border:false
            }
        }
        addBorder = () => {
            this.setState({border:true});
        };

        removeBorder = () => {
            this.setState({border:false});
        };
        render() {
            console.log(this.props);
            const border = this.state.border ? "nice-border" : "";
            return <div onMouseEnter={this.addBorder} onMouseLeave={this.removeBorder} className={border}><ReactComponent {...this.props}/></div>
        }
    }
};

export default Border;