import React from "react";
import "./Loader.css";

// High order component that returns loading indicator in case the 'propName' is empty/not define
const Loader = function (ReactComponent, propName) {
    return class Loader extends React.Component {
        isEmpty(prop) {
            return (
                prop === null ||
                prop === undefined ||
                prop.length === 0
            )
        }
        render() {
            return (this.isEmpty(this.props[propName]) ? <div className="loader"/> : <ReactComponent {...this.props}/>)
        };
    }
};

export default Loader;